#!/bin/bash

# load helper functions
source "$CI_PROJECT_DIR/deploy-scripts/utils/load.sh"

output_header "PRE DEPLOYMENT CHECK"
if [[ ! $DEPLOY_SSH_ADDRESS || ! $DEPLOY_PATH_SILVERSTRIPE || ! $DEPLOY_PATH_REACT ]]; then
  output_status "the environment variables DEPLOY_SSH_ADDRESS, DEPLOY_PATH_SILVERSTRIPE, DEPLOY_PATH_REACT are mandatory. At least one is empty!"
  exit 1
fi

output_header "START ${CI_ENVIRONMENT_NAME^^} DEPLOYMENT"

# make scripts executable
output_header "MAKE ALL DEPLOY SCRIPTS EXECUTABLE"
chmod +x ./*
success_check "done"