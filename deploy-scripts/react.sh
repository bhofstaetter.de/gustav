#!/bin/bash

# load helper functions
source "$CI_PROJECT_DIR/deploy-scripts/utils/load.sh"

# start script, go into react folder
output_header "STARTING REACT DEPLOY SCRIPT"
cd "$REACT_DIR"
success_check "$REACT_DIR"

# install dependencies
output_header "YARN INSTALL"
yarn install
success_check "installation successful"

# build react app
output_header "YARN BUILD"
yarn build
success_check "build successful"

# zip react app
output_header "ZIP REACT APP"
ZIP_NAME="$CI_ENVIRONMENT_NAME-react.zip"
zip "$ZIP_NAME" build -r
success_check "zipping successful: $ZIP_NAME"

# upload zip file
UPLOAD_PATH="$DEPLOY_PATH_REACT/deploy/"
file_upload "$ZIP_NAME" "$UPLOAD_PATH"

# combine multiple files into one remote script
output_header "CREATE REMOTE SCRIPT"
REMOTE_SCRIPT_NAME=remote-script.sh
create-remote-script "$REMOTE_SCRIPT_NAME" "$DEPLOY_SCRIPTS_DIR" "$DEPLOY_SCRIPTS_DIR/react-remote.sh"
success_check "remote script creation successful"

# execute remote script
REMOTE_ENV_VARS="
  export DEPLOY_PATH_REACT=$DEPLOY_PATH_REACT
  export DEPLOY_ENV_PATH_ADDITIONS=$DEPLOY_ENV_PATH_ADDITIONS
  export CI_ENVIRONMENT_NAME=$CI_ENVIRONMENT_NAME
"

remote_script_exec "$REMOTE_SCRIPT_NAME" "$REMOTE_ENV_VARS"