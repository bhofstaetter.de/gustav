# $1 message | $2 == 1 => sub header
function output_header {
  echo
  if [[ $2 ]]; then
    echo "## $1"
  else
    echo "### $1"
  fi
}

# $1 message | $2 == 1 => sub status
function output_status {
  if [[ $2 ]]; then
    echo "=> $1"
  else
    echo "==> $1"
  fi
}