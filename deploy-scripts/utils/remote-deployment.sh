function prepare_deployment {
  DEPLOY_PATH="$1"
  PATH_STAGING="$2"
  PATH_PRODUCTION="$3"
  PATH_BACKUP="$4"
  DEPLOY_ZIP="$5"
  DEPLOY_ZIP_PATH="$6"

  # delete former deployment
  output_header "delete former deployment" 1
  rm -rf "$DEPLOY_PATH"
  success_check "$DEPLOY_PATH deleted" 1

  # ensure folder structure exists
  output_header "ensure folder structure exists" 1
  mkdir -p "$DEPLOY_PATH"
  success_check "$DEPLOY_PATH exists" 1
  mkdir -p "$PATH_STAGING"
  success_check "$PATH_STAGING exists" 1
  mkdir -p "$PATH_PRODUCTION"
  success_check "$PATH_PRODUCTION exists" 1
  mkdir -p "$PATH_BACKUP"
  success_check "$PATH_BACKUP exists" 1

  # unzip files
  output_header "unzip $DEPLOY_ZIP to $DEPLOY_PATH" 1
  unzip "$DEPLOY_ZIP_PATH" -d "$DEPLOY_PATH"
  success_check "files unzipped" 1
}

# $1 = name of unzipped folder
function move_deployed_files {
  DEPLOY_PATH="$2"
  CI_ENVIRONMENT_NAME="$3"
  DEPLOY_PATH_BASE="$4"

  # move unzipped files
  output_header "move unzipped files" 1
  mv "$DEPLOY_PATH/$1" "$DEPLOY_PATH/$CI_ENVIRONMENT_NAME-new"
  mv "$DEPLOY_PATH/$CI_ENVIRONMENT_NAME-new" "$DEPLOY_PATH_BASE/deploy/"
  rm -rf "$DEPLOY_PATH"
  mv "$DEPLOY_PATH-new" "$DEPLOY_PATH"
  success_check "files moved" 1
}

function clean_deployed_zip {
  DEPLOY_ZIP_PATH="$1"
  DEPLOY_ZIP="$2"

  # remove zip
  output_header "remove $DEPLOY_ZIP_PATH" 1
  rm -f "$DEPLOY_ZIP_PATH"
  success_check "$DEPLOY_ZIP deleted" 1
}

function moved_deployed_to_target {
  CI_ENVIRONMENT_NAME="$1"
  TARGET_PATH="$2"
  TARGET_PATH_MOVED="$3"
  DEPLOY_PATH="$4"

  # move target site
  output_header "replace $CI_ENVIRONMENT_NAME site with newly deployed version" 1
  mv "$TARGET_PATH" "$TARGET_PATH_MOVED"
  success_check "moved old site" 1

  mv "$DEPLOY_PATH" "$TARGET_PATH"

  if [[ $? -eq 0 ]]; then
    output_status "moved new version in place" 1
    output_header "${CI_ENVIRONMENT_NAME^^} DEPLOYMENT SUCCESSFUL" 1
    rm -rf "$TARGET_PATH_MOVED"
    # todo live check?
  else
    output_status "ERROR can't move new version in place" 1
    output_header "restoring old site" 1
    mv "$TARGET_PATH_MOVED" "$TARGET_PATH"
    success_check "old site restored" 1

    exit 1
  fi
}