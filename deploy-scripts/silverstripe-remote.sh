# filename containing zipped site
DEPLOY_ZIP="$CI_ENVIRONMENT_NAME-silverstripe.zip"
# path to this zip file
DEPLOY_ZIP_PATH="$DEPLOY_PATH_SILVERSTRIPE/deploy/$DEPLOY_ZIP"
# path to deploy directory
DEPLOY_PATH="$DEPLOY_PATH_SILVERSTRIPE/deploy/$CI_ENVIRONMENT_NAME"
# path to target directory where the site will be after deployment
TARGET_PATH="$DEPLOY_PATH_SILVERSTRIPE/$CI_ENVIRONMENT_NAME"
# path of the old target in last deploy step when deployed version and live version will be switched
TARGET_PATH_MOVED="$DEPLOY_PATH_SILVERSTRIPE/$CI_ENVIRONMENT_NAME-moved"

PATH_STAGING="$DEPLOY_PATH_SILVERSTRIPE/staging"
PATH_PRODUCTION="$DEPLOY_PATH_SILVERSTRIPE/production"
PATH_BACKUP="$DEPLOY_PATH_SILVERSTRIPE/backup"

BACKUP_SQL_FILE="$CI_ENVIRONMENT_NAME.sql"
BACKUP_ZIP_FILE="$CI_ENVIRONMENT_NAME.zip"
PATH_BACKUP_SQL_FILE="$PATH_BACKUP/$BACKUP_SQL_FILE"
PATH_BACKUP_ZIP_FILE="$PATH_BACKUP/$BACKUP_ZIP_FILE"

if [[ $DEPLOY_ENV_PATH_ADDITIONS ]]; then
  export PATH=$DEPLOY_ENV_PATH_ADDITIONS:$PATH
fi

# delete former deployment
# ensure folder structure exists
# unzip files
prepare_deployment "$DEPLOY_PATH" "$PATH_STAGING" "$PATH_PRODUCTION" "$PATH_BACKUP" "$DEPLOY_ZIP" "$DEPLOY_ZIP_PATH"

# move unzipped files
move_deployed_files silverstripe "$DEPLOY_PATH" "$CI_ENVIRONMENT_NAME" "$DEPLOY_PATH_SILVERSTRIPE"

# remove zip
clean_deployed_zip "$DEPLOY_ZIP_PATH" "$DEPLOY_ZIP"

# backup currently deployed site + db, override old backup
output_header "backup $CI_ENVIRONMENT_NAME site" 1

# already deployed site exists
if [[ -f "$TARGET_PATH/.env" ]]; then
  # export variables from silverstripe env file
  export $(grep -v '^#' "$TARGET_PATH/.env" | xargs)

  # backup database
  output_header "starting sql backup" 1
  mysqldump -u"$SS_DATABASE_USERNAME" -p"$SS_DATABASE_PASSWORD" --add-drop-table "$SS_DATABASE_NAME" > "$PATH_BACKUP_SQL_FILE"
  success_check "sql dump successful: $BACKUP_SQL_FILE" 1

  # backup files including create db dump
  output_header "starting files backup" 1
  zip "$PATH_BACKUP_ZIP_FILE" "$TARGET_PATH" -r
  success_check "zipping successful: $BACKUP_ZIP_FILE" 1

  # copy assets from old to new site
  output_header "copy assets from $CI_ENVIRONMENT_NAME site" 1
  rsync -auvq "$TARGET_PATH/public/assets" "$DEPLOY_PATH/public/assets"
  success_check "assets copied" 1
else
  output_status "no $CI_ENVIRONMENT_NAME site, skipping backup" 1
fi

# rebuild cache before dev/build
output_header "rebuild silverstripe cache" 1
mkdir -p "$DEPLOY_PATH/silverstripe-cache"
success_check "$DEPLOY_PATH/silverstripe-cache created" 1

# dev/build
output_header "execute dev/build" 1
cd "$DEPLOY_PATH"
./vendor/bin/sake dev/build "flush=all"

if [[ $? -eq 0 ]]; then
  output_status "dev/build successful" 1

  # rebuild cache after dev/build
  output_header "rebuild silverstripe cache" 1
  rm -rf "$DEPLOY_PATH/silverstripe-cache"
  mkdir -p "$DEPLOY_PATH/silverstripe-cache"
  success_check "$DEPLOY_PATH/silverstripe-cache created" 1

  if [[ -f "$PATH_BACKUP_SQL_FILE" ]]; then
    # remove db dump
    output_header "deleting sql backup" 1
    rm -f "$PATH_BACKUP_SQL_FILE"
    success_check "successful" 1
  fi

  moved_deployed_to_target "$CI_ENVIRONMENT_NAME" "$TARGET_PATH" "$TARGET_PATH_MOVED" "$DEPLOY_PATH"
else
  output_status "ERROR dev/build failed" 1

  if [[ -f "$PATH_BACKUP_SQL_FILE" ]]; then
    # import db dump
    output_header "restoring $CI_ENVIRONMENT_NAME site database" 1
    mysql -u"$SS_DATABASE_USERNAME" -p"$SS_DATABASE_PASSWORD" "$SS_DATABASE_NAME" < "$PATH_BACKUP_SQL_FILE"
    success_check "database restored successful" 1

    # remove db dump
    output_header "deleting sql backup" 1
    rm -f "$PATH_BACKUP_SQL_FILE"
    success_check "successful" 1
  fi

  exit 1
fi