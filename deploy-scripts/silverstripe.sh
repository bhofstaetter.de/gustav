#!/bin/bash

# load helper functions
source "$CI_PROJECT_DIR/deploy-scripts/utils/load.sh"

# start script, go into silverstripe folder
output_header "STARTING SILVERSTRIPE DEPLOY SCRIPT"
cd "$SS_DIR"
success_check "$SS_DIR"

# delete env file
output_header "DELETING LOCAL .ENV FILE(S)"
if [[ -f .env ]]; then
  rm -f .env
  success_check ".env file deleted"
else
  output_status "no local env file found"
fi

if [[ "$CI_ENVIRONMENT_NAME" == "staging" ]]; then
  if [[ -f .env.production ]]; then
    output_sub_header "DELETING .ENV.PRODUCTION FILE"
    rm -f .env.production
    success_check ".env.production file deleted"
  fi

  if [[ -f .env.staging ]]; then
    output_header "CREATING .ENV FILE FOR DEPLOYMENT (.ENV.STAGING)"
    mv .env.staging .env
    success_check ".env file created"
  fi
elif [[ "$CI_ENVIRONMENT_NAME" == "production" ]]; then
  if [[ -f .env.staging ]]; then
    output_sub_header "DELETING .ENV.STAGING FILE"
    rm -f .env.staging
    success_check ".env.staging file deleted"
  fi

  if [[ -f .env.production ]]; then
    output_header "CREATING .ENV FILE FOR DEPLOYMENT (.ENV.PRODUCTION)"
    mv .env.production .env
    success_check ".env file created"
  fi
fi

# if still no .env file. check for file from variable
if [[ ! -f .env ]]; then
    output_header "CREATING .ENV FILE FROM VARIABLE (\$DOTENV_${CI_ENVIRONMENT_NAME^^})"
    DOTENV_FILE_VARIABLE='DOTENV_'${CI_ENVIRONMENT_NAME^^}

    if [[ ${!DOTENV_FILE_VARIABLE} ]]; then
      cat ${!DOTENV_FILE_VARIABLE} > .env
    else
      output_status "ERROR no .env file or env-file variable provided"
      exit 1
    fi
fi

# install dependencies
output_header "COMPOSER INSTALL"
composer install --ignore-platform-reqs --no-dev --no-progress --no-ansi --no-interaction
success_check "composer installation successful"

if [[ -f package.json ]]; then
  output_header "YARN INSTALL"
  yarn install
  success_check "yarn installation successful"

  # build frontend
  output_header "YARN BUILD"
  yarn build
  success_check "yarn build successful"
fi

# zip silverstripe project
output_header "ZIP SILVERSTRIPE PROJECT"
ZIP_NAME="$CI_ENVIRONMENT_NAME-silverstripe.zip"
cd ../
zip "$ZIP_NAME" silverstripe -r
success_check "zipping successful: $ZIP_NAME"

# upload zip file
UPLOAD_PATH="$DEPLOY_PATH_SILVERSTRIPE/deploy/"
file_upload "$ZIP_NAME" "$UPLOAD_PATH"

# combine multiple files into one remote script
output_header "CREATE REMOTE SCRIPT"
REMOTE_SCRIPT_NAME=remote-script.sh
create-remote-script "$REMOTE_SCRIPT_NAME" "$DEPLOY_SCRIPTS_DIR" "$DEPLOY_SCRIPTS_DIR/silverstripe-remote.sh"
success_check "remote script creation successful"

# execute remote script
REMOTE_ENV_VARS="
  export DEPLOY_PATH_SILVERSTRIPE=$DEPLOY_PATH_SILVERSTRIPE
  export DEPLOY_ENV_PATH_ADDITIONS=$DEPLOY_ENV_PATH_ADDITIONS
  export CI_ENVIRONMENT_NAME=$CI_ENVIRONMENT_NAME
"

remote_script_exec "$REMOTE_SCRIPT_NAME" "$REMOTE_ENV_VARS"